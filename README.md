# Максимально упрощенный пример Telegram бота на Python3

Сперва регистрируем бота, как сделать можно узнать по этой ссылке [как зарегистрировать телегам бота](https://kb.selectel.ru/docs/selectel-cloud-platform/serverless/instructions/how_to_create_a_telegram_bot/#зарегистрировать-нового-бота-в-telegram)

теперь устанавливаем зависимость бота:
```bash
pip install python-telegram-bot --upgrade
```
далее в файле по пути src/constants.py задаем константу TOKEN полученным при регистрации бота токеном
```python
TOKEN = 'enter_you_token_here'
```
ну и запускаем бота коммандой 
```bash
py src/main.py
```
![img](https://sun6-13.userapi.com/c852216/v852216311/1b877c/juHU2uY1S1k.jpg)

проврить бота можно просто открыв с ним чат и написав ему что нибудь, например `/start`

Что бы остановить бота просто нажми `CTRL+C`

## Как смотреть базу данных

Что бы посмотреть что записалось в базу можно скачать [SQLite Studio](https://sqlitestudio.pl/)

## Дополнительно:

Вся документация взята из github разработчиков библиотеки https://github.com/python-telegram-bot/python-telegram-bot