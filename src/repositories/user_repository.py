import logging
import sqlite3
import threading
from models.user import User

class UserRepository(object):
    def __init__(self):
        self._connection = sqlite3.connect("bot.db", check_same_thread=False)
        self._create_tables()

    def _create_tables(self):
        create_user_table_query = """CREATE TABLE IF NOT EXISTS users(
            id INTEGER PRIMARY KEY,
            user_login varchar(400) not null
        )"""
        self._connection.cursor().executescript(create_user_table_query).close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._connection.close()

    def save_user_if_not_exists(self, user: User):
        insert_query = "INSERT OR IGNORE INTO users(id, user_login) VALUES(?, ?)"
        cursor = self._connection.cursor()
        cursor.execute(insert_query, (user.id, user.user_login))
        self._connection.commit()
        cursor.close()

    def get_user(self, id: int = None, user_login: str = None) -> User:
        key, value = ("id", id) if id is not None else ("user_login", user_login)
        select_query = f"select * from users where {key} = ?"
        try:
            cursor = self._connection.cursor()
            id, user_login = cursor.execute(select_query, (value, )).fetchone()
            cursor.close()
            return User(id, user_login)
        except:
            return None
        
        