# импортим всякое говно по документации
import logging
from telegram.ext.filters import Filters
from telegram.ext.messagehandler import MessageHandler
import constants
from telegram.ext import Updater
from telegram import Update, chat
from telegram.ext import CallbackContext
from telegram.ext import CommandHandler
from models.user import User
from repositories.user_repository import UserRepository

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)

# Создаем основые объекты бота. Updater будет получать обновления с сервера телеги, а диспатчер будет выполнять функции переданные в него
# при получении сообщения от бота с соотвествующей коммандой например /start выполнит функцию def start
updater = Updater(token=constants.TOKEN)
dispatcher = updater.dispatcher
# создаем объект репозитория, он будет писать наши результаты в Sqlite базу
repo = UserRepository()

# обработчик комманды /start, аргументы опередены сигнатурой делегата который принимает handler, тупо пиши их так, а он тебе передаст эти объекты сюда сам.
def start(update: Update, context: CallbackContext):
    repo.save_user_if_not_exists(User(update.effective_user.id, update.effective_user.username))
    context.bot.send_message(chat_id=update.effective_chat.id, text=f"Hello {update.effective_user.username} i'am your bot!")

def hello(update: Update, context: CallbackContext):
    user = repo.get_user(id=update.effective_user.id)
    if (user is not None):
        context.bot.send_message(chat_id=update.effective_chat.id, text=f'I happy see you again {user.user_login} your id is {user.id}')
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text='Sorry, but i dont know who you are :-(')

def unknown(update: Update, context: CallbackContext):
    context.bot.send_message(chat_id=update.effective_chat.id, text="Sorry, I dont undestand this message!")

def error(update: Update, context: CallbackContext):
    logging.error(context.error)
    context.bot.send_message(chat_id=update.effective_chat.id, text="Ooop's an error occured :-(")

# создаем объект который вызывает делегат по комманде, можно добавить такой же для других функций
start_handler = CommandHandler('start', start)
# Обработчик приветсвия заргеистрированного пользователя
hello_handler = CommandHandler('hello', hello)
unknown_command_handler = MessageHandler(Filters.command, unknown)
unknown_message_handler = MessageHandler(Filters.text, unknown)
# записываем его в экземпляр объекта который будет это все маршрутить
dispatcher.add_handler(start_handler)
dispatcher.add_handler(hello_handler)
# Обработчик неизвестных комманд
dispatcher.add_handler(unknown_command_handler)
# Обработчик неизвестных сообщений
dispatcher.add_handler(unknown_message_handler)
# А это пример анонимной лямбды, вообще такие длинные делать не стоит
dispatcher.add_error_handler(error)



# запускаем бота, он будет жить в отдельном потоке
updater.start_polling()

# ну и блокируем закрытие по завершению через метод, внутри какой то eventLoop наверное, лень было читать
updater.idle()

repo._connection.close() # Можно так, но вообще вызывать приватные филды так себе затея