class User(object):
    def __init__(self, id, user_login):
        self._id = id
        self._user_login = user_login

    @property
    def id(self):
        return self._id

    @property
    def user_login(self):
        return self._user_login
    
    